<?php
if (!defined('_PS_VERSION_'))
{
  exit;
}

include_once dirname(__FILE__).'/classes/agv-saga-api.php';

class Agvsaga extends Module
{
  /**
  * Initiate module configuration.
  * 
  * @return void
  */
  public function __construct()
  {
    $this->name                     = 'agvsaga';
    $this->tab                      = 'billing_invoicing';
    $this->version                  = '0.9';
    $this->author                   = 'AGVision Software';
    $this->need_instance            = 1;
    $this->ps_versions_compliancy   = array('min' => '1.6', 'max' => _PS_VERSION_);
    $this->bootstrap                = true;

    parent::__construct();

    $this->displayName      = $this->l('AGV Saga');
    $this->description      = $this->l('Aceast modul facilitează exportul facturilor în programul de contabilitate Saga Soft.');
    $this->confirmUninstall = $this->l('Ești sigur că dorești să dezinstalezi modulul AGV Saga?');

    $this->agvSagaApi = new AgvSagaApi;

    if (!Configuration::get('AGV_SAGA_REGISTERED')) {
        $this->warning = $this->l('Modulul AGV Saga nu a fost înregistrat corect. Vă rugăm contactați alin@agvision.ro.');
    }
  }

  public function getContent()
  {
      $output = null;
  
      if (Tools::isSubmit('submit' . $this->name))
      {
          $my_module_name = strval(Tools::getValue('MYMODULE_NAME'));

          if (!$my_module_name
            || empty($my_module_name)
            || !Validate::isGenericName($my_module_name))
              $output .= $this->displayError($this->l('Invalid Configuration value'));
          else
          {
              Configuration::updateValue('MYMODULE_NAME', $my_module_name);
              $output .= $this->displayConfirmation($this->l('Settings updated'));
          }
      }
      return $output . $this->displayForm();
  }

  public function displayForm()
  {
      // Get default language
      $default_lang = (int) Configuration::get('PS_LANG_DEFAULT');
  
      // Init Fields form array
      $fields_form[0]['form'] = array(
          'legend' => array(
              'title' => $this->l('Settings'),
          ),
          'input' => array(
              array(
                  'type'     => 'text',
                  'label'    => $this->l('Configuration value'),
                  'name'     => 'MYMODULE_NAME',
                  'size'     => 20,
                  'required' => true
              )
          ),
          'submit' => array(
              'title' => $this->l('Save'),
              'class' => 'btn btn-default pull-right'
          )
      );
  
      $helper = new HelperForm();
  
      // Module, token and currentIndex
      $helper->module = $this;
      $helper->name_controller = $this->name;
      $helper->token = Tools::getAdminTokenLite('AdminModules');
      $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
  
      // Language
      $helper->default_form_language = $default_lang;
      $helper->allow_employee_form_lang = $default_lang;
  
      // Title and toolbar
      $helper->title = $this->displayName;
      $helper->show_toolbar = true;        // false -> remove toolbar
      $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
      $helper->submit_action = 'submit'.$this->name;
      $helper->toolbar_btn = array(
          'save' =>
          array(
              'desc' => $this->l('Save'),
              'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
              '&token='.Tools::getAdminTokenLite('AdminModules'),
          ),
          'back' => array(
              'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
              'desc' => $this->l('Back to list')
          )
      );
  
      // Load current value
      $helper->fields_value['MYMODULE_NAME'] = Configuration::get('MYMODULE_NAME');
  
      return $helper->generateForm($fields_form);
  }

  /**
     * Generate random key for Shop.
     *
     * @param int $length
     * @return void
     */
    public function generateKey($length)
    {
        return bin2hex(openssl_random_pseudo_bytes($length));
    }

  /**
   * Register new client to API. Gets called on module installation.
   *
   * @return boolean
   */
  public function install()
  {    
    if (get_option('agv_saga_registered') != 1) {
        // Register shop
        $data = [];
        $data['platform']       = 3;
        $data['key']            = $this->generateKey(10);
        $data['version']        = Configuration::get('PS_INSTALL_VERSION');
        $data['module_version'] = $agvSaga->version;
        $data['config_name']    = Configuration::get('PS_SHOP_NAME');
        $data['config_email']   = Configuration::get('PS_SHOP_EMAIL');

        $register = $this->agvSagaApi->call('register', $data);

        if ($register['httpCode'] === 201) {
            Configuration::updateValue('AGV_SAGA_REGISTERED', 1);
            Configuration::updateValue('AGV_SAGA_KEY', $data['key']);

            return true;
        } elseif ($register['httpCode'] === 409) {
            // Shop already created
            return true;
        } else {
          return false;
        }
    }
    return true;
  }

  /**
   * Do not deregister client to API. Gets called on module uninstallation.
   *
   * @return boolean
   */
  public function uninstall()
  {
    if (!parent::uninstall()) {
      return false;
    }

    return true;
  }
}